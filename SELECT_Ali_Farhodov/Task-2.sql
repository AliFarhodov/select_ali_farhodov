with PopularMovies as (
    select
        f.film_id,
        f.title as movie_title,
        COUNT(r.rental_id) as rental_count
    from film as f
    join inventory as i on f.film_id = i.film_id
    join rental as r on i.inventory_id = r.inventory_id
    group by f.film_id
    order by rental_count desc 
    limit 5
),
MovieRatings as (
    select
        f.film_id,
        f.title as movie_title,
        case
            when f.rating = 'G' then '0+'
            when f.rating = 'PG' then '7+'
            when f.rating = 'PG-13' then '13+'
            when f.rating = 'R' then '17+'
            when f.rating = 'NC-17' then '17+'
            else 'Unknown'
        end as age_category
    from film as f
)
select
    pm.movie_title,
    pm.rental_count,
    age_category as expected_age
from PopularMovies as pm
join MovieRatings as mr on pm.film_id = mr.film_id
