with ActorLastActed as (
    select
        a.actor_id,
        a.first_name,
        a.last_name,
        max(f.release_year) as last_act_year
    from
        actor as a
    join
        film_actor as fa on a.actor_id = fa.actor_id
    join
        film as f on fa.film_id = f.film_id
    group by 
        a.actor_id, a.first_name, a.last_name
)
, LatestReleaseYear as (
    select 
        max(f.release_year) as latest_year
    from
        film as f
)
select
    actor_id,
    CONCAT(first_name, ' ', last_name) as full_name,
    last_act_year
from
    ActorLastActed as ala, LatestReleaseYear as lry
where
    ala.last_act_year < lry.latest_year
order by
    ala.last_act_year;
