with Store_Revenue as (
    select
        store_id,
        s.staff_id,
        s.first_name || ' ' || s.last_name as full_name,
        sum(p.amount) as total_revenue
    from staff as s
    join payment as p on s.staff_id = p.staff_id
    where extract(year from p.payment_date) = 2017
    group by s.store_id, s.staff_id, full_name
),
Rank_Revenue as (
    select
        store_id,
        staff_id,
        full_name,
        total_revenue,
        row_number() over (partition by store_id order by total_revenue desc) as ranking
    from Store_Revenue
)
select store_id, staff_id, full_name, total_revenue
from Rank_Revenue
where ranking = 1;
